This repository is deprecated.


Autoconfig-DMN is a DMN modeler/solver which combines [dmn-js](https://github.com/bpmn-io/dmn-js) and the [Interactive Consultant interface](https://gitlab.com/krr/autoconfig3).

Interactive\_Consultant is an interactive consultant based on logic. It uses the Z3 SMT solver.



