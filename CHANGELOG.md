# Changelog

DEPRECATED on 202202-01  !!

## [Unreleased]
* Covid example
* open local file with `/?file=spec.idp`
* MR !51: switching between reading and formula view now works for explanations too

## [Released]
* ctrl-S in IDE runs the code
* MR !28: more finegrained resets and better behavior when not using active propagation
* refactor to simplify meta's response
* MR !41: support touch devices
* refactor to simplify the amount of status codes and their usage

* MR !25: Remove repetition in header symbol tiles
* add support for date fields


## [0.5.3]
* add Help / Cheatsheet


## [0.5.2] - 2020-09-15
### API
* add ENV_UNIV status (for universal environmental rule)
* add `propagated` to /meta's response

### GUI
* File / New
* File / Share via URL

## [0.4.1] - 2020-04-01
### GUI
* fix environmental consequences
* avoid duplicates in explain

## [0.4.0] - 2020-03-16
### API
* add EXPANDED status
* use 'relevant' field, not 'RELEVANT' value in Get /eval response
* remove 'ct', 'cf' from /eval Request
* add global variables for environmental vs decision variables
### GUI
* green box around decision symbols
* use transparency for irrelevant symbols, unexpected value of sentences to be verified


## [0.3.2] - 2020-01-31
* Improved color coding of buttons.
* Hide irrelevant sentences / symbols (for theories without definitions).
* More compact symbol panel: header is not shown if only one sentence is shown.


## [0.1.0] - 2019-12-10
Initial release
