import { Component, OnInit } from '@angular/core';
import {IdpService} from '../../services/idp.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-background-editor',
  templateUrl: './background-editor.component.html',
  styleUrls: ['./background-editor.component.css']
})
export class BackgroundEditorComponent {

  editorOptions = {theme: 'IDP', language: 'IDP'};

  public onInitEditor(editor: any) {
    this.idpService.backgroundEditor = editor;
    }

  constructor(public idpService: IdpService, private messageService: MessageService) {
    this.messageService.add({});
  }

  public type(char: string) {
    this.idpService.backgroundEditor.trigger('keyboard', 'type', {text: char});
  }

}
